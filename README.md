# Drafts für die abzugebenden Dokumente

## Anforderungen

Anforderungen für alle Dokumente ist XeLaTeX, beispielsweise aus dem package `texlive-xetex` (Ubuntu). <br>
Genauso werden die Fonts Caladea und FuraCode Nerd Font. Alle der Folgenden können in LaTeX, sofern auf dem System vorhanden, stattdessen benutzt werden: 
 - Fira Mono
 - Fira Code
 - Fura Code (is eigentlich dasselbe wie Fira Code, heißt aber manchmal anders)
 - FuraCode Mono Nerd Font
